﻿using UnityEngine;
using System.Collections;

public class Enemy2Shoot : MonoBehaviour {
	private GameObject player;
	private float dist;
	private Rigidbody clone;
	private Vector3 add;
	private Vector3 withouty;
	public Rigidbody projectile;
	
	private float timePassed_ = 0.0f;
	public float timeToFire_;
	
	Transform myTR;
	
	// Use this for initialization
	void Awake () {
		
		myTR = GetComponent<Transform> ();
		player = GameObject.FindWithTag ("Player");
		dist = 20 * GameManager.Instance.difficulty;
		timeToFire_ = 2 - GameManager.Instance.difficulty;
	}
	
	// Update is called once per frame
	void Update () {
		fire ();
		
	}
	void fire()
	{
		RaycastHit ray;
		
		Vector3 origin = myTR.position;
		Vector3 direction = (player.transform.position - myTR.position).normalized;
		Debug.DrawRay (origin, direction, Color.red);
		Physics.Raycast (origin, direction, out ray, dist);
		
		if (ray.collider != null && ray.collider.gameObject.tag == "Player" && ray.distance <= dist) 
		{
			timePassed_ += Time.deltaTime;
			if (timePassed_ >= timeToFire_)
			{
				timePassed_=0.0f;
				Vector3 ri = player.transform.position;
				
				withouty = new Vector3(ri.x, 1.079641f, ri.z);
				myTR.forward = withouty - myTR.position;
				
				add = myTR.position + (myTR.forward*2.4f);
				clone = Instantiate(projectile,add , myTR.rotation) as Rigidbody;
				clone.velocity = direction*20;
			}
		}
		
		
	}
}