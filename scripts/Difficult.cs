﻿using UnityEngine;
using System.Collections;

public class Difficult : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void easy()
	{
		GameManager.Instance.difficulty = 0.7f;
	}
	public void normal()
	{
		GameManager.Instance.difficulty = 1.0f;
	}
	public void hard()
	{
		GameManager.Instance.difficulty = 1.5f;
	}
}
