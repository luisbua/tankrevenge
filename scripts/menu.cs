﻿using UnityEngine;
using System.Collections;

public class menu : MonoBehaviour {

	public void SceneStart()
	{
		Application.LoadLevel ("Level_001");
		GameManager.Instance.num_enemy_initial = 0;
		GameManager.Instance.num_enemy = 0;
	}
	public void SceneOptions()
	{
		Application.LoadLevel ("Settings");
	}
	public void SceneCredits()
	{
		Application.LoadLevel ("Credits");
	}
	public void SceneInstructions()
	{
		Application.LoadLevel ("Instructions");
	}
	public void SceneExit()
	{
		Application.Quit();
		//UnityEditor.EditorApplication.isPlaying = false;
	}
}
