﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	
	public GameObject shellExplosion;
	private GameObject boom;
	public AudioSource sound;
	// Use this for initialization
	void Start () {

        sound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnCollisionEnter(Collision  colision) {
		
		boom = Instantiate(shellExplosion, transform.position, transform.rotation) as GameObject;
        sound.Play();
		Destroy(gameObject, 0.05f);
		Destroy(boom,1.5f);
	}
}
