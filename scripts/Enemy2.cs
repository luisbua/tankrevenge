﻿using UnityEngine;
using System.Collections;

public class Enemy2 : MonoBehaviour {

	private	NavMeshAgent agent;
	private Vector3 destino;
	private GameObject player;
	public GameObject tankexplosion;
	private GameObject boom;
    private AudioSource sound;
	private Transform myTR;
	
	
	// Use this for initialization
	void Awake() {
		agent = GetComponent<NavMeshAgent> ();
		player = GameObject.FindWithTag ("Player");
        sound = GetComponent<AudioSource>();
		myTR = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
			movement ();

	}
	void movement(){
		if (myTR.transform.position.y != 0) {
			myTR.position = new Vector3(myTR.transform.position.x,0.0f,myTR.transform.position.z);
		}
		destino = player.transform.position;
		agent.SetDestination(destino);
	}
	void OnCollisionEnter(Collision colision)
	{
		
		if (colision.gameObject.tag == "ShellPlayer") {
			
			GameManager.Instance.num_enemy--;
			boom = Instantiate(tankexplosion, transform.position, transform.rotation) as GameObject;
            sound.Play();
			Destroy(gameObject, 0.05f);
			Destroy(boom,1.5f);
		}
	}
}
