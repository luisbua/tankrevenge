﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

			
	public int num_enemy;
	public int num_enemy_initial;
	public int num_level;
	public int num_lives;
	public float volume_slider;
    public float difficulty;
    public float shakeDuration;
    public float shotsFired;
    public bool phone;
    public bool canIPause;
    public bool winCancel;

    /*
     * lvlScore[0] = overall
     * lvlScore[1] = score for lvl 1
     * lvlScore[2] = score for lvl 2
     * ...
     */
    public float[] highScore = new float[21];

		//Singleton lazy
		private static GameManager instance_;
		
		public static GameManager Instance
		{
			get{
				
				if(instance_ == null)
				{
					GameObject go = new GameObject("GameManager");
					instance_ = go.AddComponent<GameManager>();
					DontDestroyOnLoad (instance_.gameObject);
				}
				
				return instance_;
			}
		}
        void Awake()
        {
            num_enemy_initial = 0;
            num_lives = 3;
            volume_slider = 1.0f;
            difficulty = 1.0f;
            shakeDuration = 0.0f;
            shotsFired = 0.0f;
            phone = false;
            canIPause = true;
            winCancel = false;
        }
		void Update ()
		{
		}
}