﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class finish_fail : MonoBehaviour {

    public Text text;
    public Text textShadow;
    public Text text1;
    public Text text2;
    public Text text3;
    public float bestScore = 0;
    public float allTime = 0;
	private float timer = 0;
	public admobScript ad_banner;

	// Use this for initialization
	void Start () {
        // Calculates the level score and saves it in the game manager array
        GameManager.Instance.highScore[GameManager.Instance.num_level] = 0;
        GameManager.Instance.highScore[GameManager.Instance.num_level] = Mathf.Round((GameManager.Instance.num_enemy_initial - GameManager.Instance.num_enemy) * 100);

        // Saves in highscore[0] this sesion acumulated scores
        GameManager.Instance.highScore[0] = 0;
        for (int i = 1; i <= GameManager.Instance.num_level; i++)
        {
            GameManager.Instance.highScore[0] += GameManager.Instance.highScore[i];
        }

        /*
         * At every lvl compares the best score of all time with this sesion's best score
         * If there is no record, it creates one
        */
        if (PlayerPrefs.HasKey("bestScore"))
        {
            allTime = PlayerPrefs.GetFloat("bestScore");
            if (GameManager.Instance.highScore[0] > allTime)
            {
                allTime = GameManager.Instance.highScore[0];
                PlayerPrefs.SetFloat("bestScore", allTime);
                PlayerPrefs.Save();
            }
        }
        else
        {
            PlayerPrefs.SetFloat("bestScore", GameManager.Instance.highScore[0]);
            PlayerPrefs.Save();
        }

        text.text = "  Lvl " + GameManager.Instance.num_level + "        Total        All Time";
        textShadow.text = "  Lvl " + GameManager.Instance.num_level + "        Total        All Time";
        text1.text = "" + GameManager.Instance.highScore[GameManager.Instance.num_level];
        text2.text = "" + GameManager.Instance.highScore[0];
        text3.text = "" + allTime;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void FixedUpdate()
    {
        timer++;
    }
	public void SceneRetry()
	{
        if (timer > 120)
        {
            GameManager.Instance.num_enemy = 0;
            GameManager.Instance.num_enemy_initial = 0;
            Application.LoadLevel(GameManager.Instance.num_level);
            ad_banner.HideBanner();
        }
	}
	public void SceneMenu()
	{
        if (timer > 120)
        {
            GameManager.Instance.num_enemy = 0;
            GameManager.Instance.num_enemy_initial = 0;
            Application.LoadLevel("menus");
            ad_banner.HideBanner();
        }
	}
}
