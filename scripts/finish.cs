﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class finish : MonoBehaviour {
	public Text text;
    public Text textShadow;
    public Text text1;
    public Text text2;
    public Text text3;
    public Text diffGreen;
    public Text diffBlack;
    public GameObject Star;
    public GameObject Star1;
    public GameObject Star2;
    public GameObject Star3;
    public GameObject Star4;
    public GameObject Star5;
    public GameObject Star6;
    private float allTime=0;
    private float numOfStars;
    private float timer = 0;
	public admobScript ad_banner;

	// Use this for initialization
	void Start () {

		//ad_banner.ShowInterstitial ();
        //Write difficulty
        if (GameManager.Instance.difficulty < 0.8) { 
            diffGreen.text = "EASY (x1)"; diffBlack.text = "EASY (x1)"; 
        }
        if (GameManager.Instance.difficulty < 1.3 && GameManager.Instance.difficulty > 0.8) {
            Star5.SetActive(true); 
            diffGreen.text = "MEDIUM (x2)"; diffBlack.text = "MEDIUM (x2)"; 
        }
        if (GameManager.Instance.difficulty > 1.3) {
            Star5.SetActive(true);
            Star6.SetActive(true); 
            diffGreen.text = "HARD (x3)"; diffBlack.text = "HARD (x3)";
        }

        // Calculates the level score and saves it in the game manager array
        GameManager.Instance.highScore[GameManager.Instance.num_level] = 0;
        GameManager.Instance.highScore[GameManager.Instance.num_level] = Mathf.Round(
            (GameManager.Instance.num_enemy_initial) * 100 * 
            GameManager.Instance.num_lives * 
            (2 * GameManager.Instance.difficulty)
            - (5 * GameManager.Instance.shotsFired));

        //if score < 100, score = 100
        if (GameManager.Instance.highScore[GameManager.Instance.num_level] < 100) GameManager.Instance.highScore[GameManager.Instance.num_level] = 100;


        // Saves in highscore[0] this sesion acumulated scores
        GameManager.Instance.highScore[0] = 0;
        for (int i = 1; i <= GameManager.Instance.num_level; i++){
            GameManager.Instance.highScore[0] += GameManager.Instance.highScore[i];
        }

        /*
         * At every lvl compares the best score of all time with this sesion's best score
         * If there is no record, it creates one
        */
        if (PlayerPrefs.HasKey("bestScore")){
            allTime = PlayerPrefs.GetFloat("bestScore");
            if (GameManager.Instance.highScore[0] > allTime){
                allTime = GameManager.Instance.highScore[0];
                PlayerPrefs.SetFloat("bestScore", allTime);
                PlayerPrefs.Save();
            }
        }
        else{
            PlayerPrefs.SetFloat("bestScore", GameManager.Instance.highScore[0]);
            PlayerPrefs.Save();
        }

        text.text = "  Lvl " + GameManager.Instance.num_level + "        Total        All Time";
        textShadow.text = "  Lvl " + GameManager.Instance.num_level + "        Total        All Time";
        text1.text = "" + GameManager.Instance.highScore[GameManager.Instance.num_level];
        text2.text = "" + GameManager.Instance.highScore[0];
        text3.text = "" + allTime;


        /*
         * Stars after completing a lvl
         * 1 star for each remaining life
         * 1 star for MED difficulty
         * 2 star for HARD difficulty
        */
        numOfStars = 0;
        if (GameManager.Instance.difficulty > 0.8f) numOfStars = 1;
        if (GameManager.Instance.difficulty > 1.0f) numOfStars = 2;
        numOfStars += GameManager.Instance.num_lives;
        //Debug.Log("vidas" + GameManager.Instance.num_lives + " - estrellas" + numOfStars);
          
	}
    void FixedUpdate()
    {
        timer++;
    }

	// Update is called once per frame
	void Update () {
        
        if (numOfStars > 0) Star.SetActive(true); 
        if (numOfStars > 1 && timer > 15)  Star1.SetActive(true); 
        if (numOfStars > 2 && timer > 30)  Star2.SetActive(true); 
        if (numOfStars > 3 && timer > 45)  Star3.SetActive(true);
        if (numOfStars > 4 && timer > 60)  Star4.SetActive(true);
	}
	public void SceneRetry()
	{
        if (timer > 120)
        {
            GameManager.Instance.num_enemy = 0;
            GameManager.Instance.num_enemy_initial = 0;
            Application.LoadLevel(GameManager.Instance.num_level);
            ad_banner.HideBanner();
        }
	}
	public void SceneMenu()
	{
        if (timer > 120)
        {
		GameManager.Instance.num_enemy = 0;
		GameManager.Instance.num_enemy_initial = 0;
		Application.LoadLevel ("menus");
		ad_banner.HideBanner ();	
     }
	}
	public void SceneNext()
	{
        if (timer > 120)
        {
            GameManager.Instance.num_enemy = 0;
            GameManager.Instance.num_enemy_initial = 0;
            Application.LoadLevel(GameManager.Instance.num_level + 1);
            ad_banner.HideBanner();
        }
		
	}
}
