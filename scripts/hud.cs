﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class hud : MonoBehaviour {

	public GameObject[] num_Enem;
	public int lives = 3;
	public Text text;
	public Text text_shadow;
	public GameObject win;
	public GameObject menu_Pause;
	public GameObject on;
	public GameObject off;
    public GameObject MovePad;
    public GameObject FirePad;
    private int[] enemyArray;

	private float timePassed_ = 0.0f;
	private float timePassed2_ = 0.0f;
	public float timeToFire_ = 2.0f;
	public float timeToFire2_ = 3.0f;
	private int change = 1;


	// Use this for initialization
	void Awake () {
        enemyArray = new int[]{0,1,2,3,3,4,6,5,6,6,6,7,7,8,8,7,8,7,8,10,10};

        GameManager.Instance.canIPause = true;
        GameManager.Instance.winCancel = false;

        if (GameManager.Instance.phone == false){
            MovePad.SetActive(false);
            FirePad.SetActive(false);
        }
        if (GameManager.Instance.phone == true){
            MovePad.SetActive(true);
            FirePad.SetActive(true);
        }

        /*
        //Forma de BUA
		GameManager.Instance.num_enemy = 0;
		GameManager.Instance.num_enemy_initial = 0;
        GameManager.Instance.shotsFired = 0;
		num_Enem = GameObject.FindGameObjectsWithTag ("Enemy");
		for (int i = 0; i < num_Enem.Length; i++) GameManager.Instance.num_enemy_initial++;
		GameManager.Instance.num_enemy = GameManager.Instance.num_enemy_initial;
        */


	}
    void Start()
    {
        GameManager.Instance.num_enemy_initial = Application.loadedLevel; 
        GameManager.Instance.num_level = Application.loadedLevel;
        GameManager.Instance.num_enemy = enemyArray[GameManager.Instance.num_level];
        

        text.text = "LEVEL " + GameManager.Instance.num_level;
        text_shadow.text = "LEVEL " + GameManager.Instance.num_level;
        AudioListener.pause = false;
    }
	
	// Update is called once per frame
	void Update () {
		timePassed_ += Time.deltaTime;
        //Debug.Log(GameManager.Instance.num_enemy);

        if (GameManager.Instance.num_enemy <= 0 && GameManager.Instance.winCancel == false)
        {
            GameManager.Instance.canIPause = false;
			timePassed2_ += Time.deltaTime;
			win.SetActive(true);
			if (timePassed2_ >= timeToFire2_) {
				Application.LoadLevel ("Finish");
			}
		}
		if (timePassed_ >= timeToFire_) {
			text.enabled = false;
			text_shadow.enabled = false;
			timePassed_ = 0;
		}

		AudioListener.volume = GameManager.Instance.volume_slider; 

	}

	public void ScenePause()
	{
        if (GameManager.Instance.canIPause == true){
            Time.timeScale = 0;
            menu_Pause.SetActive(true);
            AudioListener.pause = true;
        }

	}
	public void SceneBack()
	{
		Time.timeScale = 1;
		GameManager.Instance.num_enemy = 0;
		GameManager.Instance.num_enemy_initial = 0;
		Application.LoadLevel ("menus");

	}
	public void SceneRetry()
	{
		Time.timeScale = 1;
		GameManager.Instance.num_enemy = 0;
		GameManager.Instance.num_enemy_initial = 0;
		Application.LoadLevel (GameManager.Instance.num_level);

	}
	public void SceneResume()
	{
		Time.timeScale = 1;
		menu_Pause.SetActive(false);
		AudioListener.pause = false;
	}
	public void SceneAudio()
	{
		if (change == -1) {
			AudioListener.pause = true;
			change *= -1;
			on.SetActive(false);
			off.SetActive(true);

		}
		else if (change == 1) {
			AudioListener.pause = false;
			change *= -1;
			on.SetActive(true);
			off.SetActive(false);
		}
	}

}

