﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderController : MonoBehaviour {

	private Slider sli;

	// Use this for initialization
	void Start () {
		sli = GetComponent<Slider> ();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
	public void SceneVolume()
	{
		GameManager.Instance.volume_slider = sli.value;

	}

}
