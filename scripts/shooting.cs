﻿using UnityEngine;
using System.Collections;
using CnControls;
using UnityEngine.UI;

public class shooting : MonoBehaviour {
    
    private Vector3 where;
    private Vector3 aim;
    private Vector3 withouty;
    private Transform myTrans;
    private float mousex;
    private float mousey;
    public Rigidbody projectile;
    private Rigidbody clone;
    private Vector3 add;
	private Vector3 pos;
	private float timePassed_ = 0.0f;
	public float timeToFire_ = 0.7f;
    public Camera Cam;


	void Start() {
        myTrans = GetComponent<Transform>();
    }

	void Update () 
	{
		//TouchFire ();
        if (GameManager.Instance.phone == true)
        {
            FirePad();
        }
        else
        {
            mouseFire();
        }
    }

    private void AimAndShoot(Vector3 ri){
       
        withouty = new Vector3(ri.x, 1.079641f, ri.z);
        myTrans.forward = withouty - myTrans.position;

        GameManager.Instance.shotsFired += 1;
       // Debug.Log(GameManager.Instance.shotsFired); //How many shots are fire on each level
        add = transform.position + (transform.forward*1.4f);
        clone = Instantiate(projectile, add, transform.rotation) as Rigidbody;
        clone.velocity = transform.TransformDirection(Vector3.forward * 30);
    }
    
	private void mouseFire()
	{
        timePassed_ += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && timePassed_ >= timeToFire_) 
		{
			mousex = Input.mousePosition.x;
			mousey = Input.mousePosition.y;
            timePassed_ = 0.0f;
           // Debug.Log("x: " + mousex + "y: " + mousey);
            

            where = Cam.ScreenToWorldPoint(new Vector3(mousex, mousey, 35));
            aim = Cam.gameObject.transform.forward;
			RaycastHit ri;
			
			if (Physics.Raycast(where, aim, out ri)){
				withouty = new Vector3(ri.point.x, 1.079641f, ri.point.z);
				myTrans.forward = withouty - myTrans.position;
			}

           // Debug.Log("x: " + ri.point.x + " - z: " + ri.point.z);

            GameManager.Instance.shotsFired += 1;
			add = transform.position + (transform.forward*1.4f);
			clone = Instantiate(projectile, add, transform.rotation) as Rigidbody;
			clone.velocity = transform.TransformDirection(Vector3.forward * 30);

		}
	}
     
	private void TouchFire()
	{
		for (int i = 0; i < Input.touchCount; i++) {
			if (Input.touchCount > 0 && Input.GetTouch (i).phase == TouchPhase.Began) {
				// create ray from the camera and passing through the touch position:
				Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (i).position);
				// create a logical plane at this object's position
				// and perpendicular to world Y:
				Plane plane = new Plane (Vector3.up, new Vector3 (0, 0.5f, 0));// transform.position
				float distance = 0; // this will return the distance from the camera
				if (plane.Raycast (ray, out distance)) { // if plane hit...
					pos = ray.GetPoint (distance); // get the point
					// pos has the position in the plane you've touched
				}
				if (!(Input.GetTouch(i).position.x < 325 && Input.GetTouch(i).position.y < 325))
					AimAndShoot(pos);
				
			}
		}
		
		if (Input.GetAxis ("Jump") != 0) {
			AimAndShoot (myTrans.forward);
			
		}
	}
	private void FirePad()
	{
        timePassed_ += Time.deltaTime;
        if (timePassed_ >= timeToFire_)
        {
		    float ver = CnInputManager.GetAxis ("Vertical1");
		    float hor = CnInputManager.GetAxis ("Horizontal1");
		    if (ver == 0 && hor == 0) timePassed_= timeToFire_;
		    if (ver != 0 || hor != 0) {
		    	pos = new Vector3 (myTrans.position.x + hor, 0.0f,myTrans.position.z + ver);

				AimAndShoot(pos);
				timePassed_=0.0f;
							
			}
		}
	}
}




