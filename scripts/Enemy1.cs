﻿using UnityEngine;
using System.Collections;

public class Enemy1 : MonoBehaviour {

	private Transform myTR;
	private NavMeshAgent agent;
	private Vector3 destino;
	private float radius = 20.0f;
	private float timePassed_ = 0.0f;
	public float timeToDestroy_ = 2.0f;
	public GameObject tankexplosion;
	private GameObject boom;
    private AudioSource sound;


	// Use this for initialization
	void Start () {

		myTR = GetComponent<Transform> ();
		agent = GetComponent<NavMeshAgent> ();
        sound = GetComponent<AudioSource>();
	
	}



	// Update is called once per frame
	void Update () {

		movement ();

	}
	void movement(){

		if (myTR.transform.position.y != 0) {
			myTR.position = new Vector3(myTR.transform.position.x,0.0f,myTR.transform.position.z);
		}

		if (agent.remainingDistance <= 10.0f) {
			timePassed_ += Time.deltaTime;
			destino = new Vector3 (Random.Range (myTR.position.x - radius, myTR.position.x + radius), 
			                       Random.Range (0.4f, 0.9f), 
			                       Random.Range (myTR.position.z - radius, myTR.position.z + radius));
				
		}

		if (timePassed_ >= timeToDestroy_)
		{
			timePassed_=0.0f;
			agent.SetDestination(destino);

		}

	}
	void OnCollisionEnter(Collision colision)
	{
		
		if (colision.gameObject.tag == "ShellPlayer") {
	
			GameManager.Instance.num_enemy--;
			boom = Instantiate(tankexplosion, transform.position, transform.rotation) as GameObject;
            sound.Play();
			Destroy(gameObject, 0.05f);
			Destroy(boom,1.5f);

		}
	}

}
