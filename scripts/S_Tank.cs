﻿using UnityEngine;
using System.Collections;
using CnControls;
using UnityEngine.UI;

public class S_Tank : MonoBehaviour {
	private Transform myTransform;
	private Rigidbody myRB;

	private float speed = 0.25f;
	private float move = 0.0f;
	private float direction = 0.0f;
	public Touch tocar;
	Vector3 pos;
	Vector3 mp;
    public AudioSource sound;
	private GameObject Life1;
	private GameObject Life2;
	private GameObject Life3;
	private float timePassed2_ = 0.0f;
	public float timeToFire2_ = 3.0f;
	public GameObject tankexplosion;
	private GameObject boom;
	public AudioSource audio2;
	private GameObject lose_text;
	private GameObject win_text;
	private bool start_count;

	//private int lives = 3;

	
	
	// Use this for initialization
	void Start () 
	{
        sound = GetComponent<AudioSource>();
        GameManager.Instance.num_lives = 3;
		myTransform = GetComponent<Transform> ();
		myRB = GetComponent<Rigidbody> ();
		//tocar = GetComponent<Touch> ();
		Life1 = GameObject.Find ("Life1");
		Life2 = GameObject.Find ("Life2");
		Life3 = GameObject.Find ("Life3");
		start_count = false;
        
	}
	void Awake()
	{
		lose_text = GameObject.Find ("LOSE");
		win_text = GameObject.Find ("WIN");

		lose_text.SetActive (false);
		win_text.SetActive (false);

	}

	// Update is called once per frame
	void Update () 
	{
		if (myTransform.transform.position.y != 0) {
			myTransform.position = new Vector3(myTransform.transform.position.x,0.0f,myTransform.transform.position.z);
		}
		move = CnInputManager.GetAxis ("Vertical");
		direction = CnInputManager.GetAxis ("Horizontal");

	}

	void FixedUpdate()
	{
		moveTank ();
		//Debug.Log(timePassed2_);
		if (start_count == true) {
			timePassed2_ += Time.deltaTime;
			
		}
		if (timePassed2_ >= timeToFire2_) {
			Application.LoadLevel ("Finish_fail");  
		}
	}
	
	void moveTank()
	{
		//Tank Movement
		myRB.MovePosition (myTransform.position + new Vector3 (direction, 0.0f, move)*speed); 

		//Tank Direction
		if(move !=0 || direction != 0) 
		myTransform.forward = new Vector3 (direction, 0.0f, move);

		//Audio
		float source1 = move;
		float source2 = direction;
		if (source1 < 0) source1 *= -1;
		if (source2 < 0) source2 *= -1;
		float index = source1 + source2 + 1;
		if(index>2)index=2;
        sound.pitch = index;

	}
	void OnCollisionEnter(Collision colision)
	{
		
		if (colision.gameObject.tag == "Shell")
		{
            GameManager.Instance.num_lives -= 1;
            switch (GameManager.Instance.num_lives)
			{
				case 0: 
					{
                        GameManager.Instance.canIPause =false;
                        GameManager.Instance.winCancel = true;
                        GameManager.Instance.shakeDuration = 0.2f;
						boom = Instantiate(tankexplosion, transform.position, transform.rotation) as GameObject;
                        sound.Play();
						Destroy(boom,1.5f);
						Life1.SetActive (false);
						start_count = true;
						gameObject.transform.position = new Vector3 (50000,0.0f,50000);
						//GameManager.Instance.num_lives = 1;
                        win_text.SetActive(false);
						lose_text.SetActive(true);
						break;
					}
				case 1: 
					{
                        GameManager.Instance.shakeDuration = 0.2f;
						Life2.SetActive (false); 
						//GameManager.Instance.num_lives = 1;
						break;
					}
				case 2: 
					{
                        GameManager.Instance.shakeDuration = 0.2f;
						Life3.SetActive (false); 
						//GameManager.Instance.num_lives = 2;
						break;
					}
			}

			
		}
	}
}
